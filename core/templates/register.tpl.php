<?php
 /**
 * register.tpl.php (the sites main template)
 *
 * @package Blackcat Network
 * @author Jeremy Stevens
 * @copyright 2014 Jeremy Stevens
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */

 ?>
?>
<!DOCTYPE html>
<html>
<head>
<title>BlackCat Network</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="lib/css/style.css" />
<script src="lib/js/jquery-2.1.1.min.js"></script>
<script src="lib/js/api.min.js"></script>
<script src="lib/js/jquery.fancybox.pack.js"></script>
<script src="lib/js/openpgp.min.js"></script>
<script type="text/javascript" src="lib/js/jquery.validate.js" /></script>
<script type="text/javascript" src="lib/js/jquery-validate.bootstrap-tooltip.js" /></script>
!----ajax to verify Captacha code & form validation -->
<script type="text/javascript">
	$(function()
{
 
    //jQuery Captcha Validation
    WEBAPP = {
 
        settings: {},
        cache: {},
 
        init: function() {
 
            //DOM cache
            this.cache.$form = $('#login_form');
            this.cache.$refreshCaptcha = $('#refresh-captcha');
            this.cache.$captchaImg = $('img#captcha');
            this.cache.$captchaInput = $(':input[name="captcha"]');
 
            this.eventHandlers();
            this.setupValidation();
 
        },
 
        eventHandlers: function() {
 
            //generate new captcha
            WEBAPP.cache.$refreshCaptcha.on('click', function(e)
            {
                WEBAPP.cache.$captchaImg.attr("src","core/captcha.php");
            });
        },
 
        setupValidation: function()
        {
 
            WEBAPP.cache.$form.validate({
               onkeyup: false,
               rules: {
                    "username": {
                        "required": true
                    },
                    "email": {
                        "required": true,
                        email: true
                    },
                    "password": {
                        "required": true
                    },
                     "password2": {
                        "required": true
                    },
                    "captcha": {
                        "required": true,
                        "remote" :
                        {
                          url: 'core/validate.php',
                          type: "post",
                          data:
                          {
                              code: function()
                              {
                                  return WEBAPP.cache.$captchaInput.val();
                              }
                          }
                        }
                    }
                },
                messages: {
                    "username": "please chose a username",
                    "email": "please enter a valid email address",
                    "password": "Please chose a password.",
                    "password2": "please retype your password",
                    "captcha": {
                        "required": "Please enter the verifcation code. -->",
                        "remote": "Verication code incorrect, please try again."
                    }
                },
               // code removed here 
 
            });
 
        }
 
    }
 
    WEBAPP.init();
 
});
	
</script>

<script>
<script type="text/javascript" src="lib/js/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#username').keyup(function(){ // Keyup function for check the user action in input
		var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
		var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
		if(Username.length > 2) { // check if greater than 2 (minimum 3)
			UsernameAvailResult.html('Loading...'); // Preloader, use can use loading animation here
			var UrlToPass = 'action=username_availability&username='+Username;
			$.ajax({ // Send the username val to another check_username.php using Ajax in POST menthod
			type : 'POST',
			data : UrlToPass,
			url  : 'core/check_username.php',
			success: function(responseText){ // Get the result and asign to each cases
				if(responseText == 0){
					UsernameAvailResult.html('<span class="success">Username name available</span>');
				}
				else if(responseText > 0){
					UsernameAvailResult.html('<span class="error">Username already taken</span>');
				}
				else{
					alert('Problem with sql query');
				}
			}
			});
		}else{
			UsernameAvailResult.html('Enter atleast 3 characters');
		}
		if(Username.length == 0) {
			UsernameAvailResult.html('');
		}
	});
	
	$('#password, #username').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
		if (e.which == 32) {
			return false;
  		}
	});
	$('#password').keyup(function() { // As same using keyup function for get user action in input
		var PasswordLength = $(this).val().length; // Get the password input using $(this)
		var PasswordStrength = $('#password_strength'); // Get the id of the password indicator display area
		
		if(PasswordLength <= 0) { // Check is less than 0
			PasswordStrength.html(''); // Empty the HTML
			PasswordStrength.removeClass('normal weak strong verystrong'); //Remove all the indicator classes
		}
		if(PasswordLength > 0 && PasswordLength < 4) { // If string length less than 4 add 'weak' class
			PasswordStrength.html('weak');
			PasswordStrength.removeClass('normal strong verystrong').addClass('weak');
		}
		if(PasswordLength > 4 && PasswordLength < 8) { // If string length greater than 4 and less than 8 add 'normal' class
			PasswordStrength.html('Normal');
			PasswordStrength.removeClass('weak strong verystrong').addClass('normal');			
		}	
		if(PasswordLength >= 8 && PasswordLength < 12) { // If string length greater than 8 and less than 12 add 'strong' class
			PasswordStrength.html('Strong');
			PasswordStrength.removeClass('weak normal verystrong').addClass('strong');
		}
		if(PasswordLength >= 12) { // If string length greater than 12 add 'verystrong' class
			PasswordStrength.html('Very Strong');
			PasswordStrength.removeClass('weak normal strong').addClass('verystrong');
		}
	});
});
</script>
<style type="text/css">
.username_avail_result{
	display:block;
	width:180px;
}
.success{
	color:#009900;
}
.error{
	color:#F33C21;
}
.talign_right{
	text-align:right;
}
.username_avail_result{
	display:block;
	width:180px;
}
.password_strength {
	display:block;
	width:180px;
	padding:3px;
	text-align:center;
	color:#333;
	font-size:12px;
	backface-visibility:#FFF;
	font-weight:bold;
}
/* Password strength indicator classes weak, normal, strong, verystrong*/
.password_strength.weak{
	background:#e84c3d;
}
.password_strength.normal{
	background:#f1c40f;
}
.password_strength.strong{
	background:#27ae61;
}
.password_strength.verystrong{
	background:#2dcc70;
	color:#FFF;
}
</style>

</head>


 <body>
	<div class="main-bg"></div>
	<div class="main-logo">BlackCat <span>networks</span></div>
	<div class="auth-box">
		<div class="auth-box-inner">
			<a class="to-register" href="index.php">Login?</a>
			<form id="login_form" action="index.php?action=process" method="post">
      <input type='hidden' name='csrfmiddlewaretoken' value='yoicYrOQnCmSdNjupbjmId6sd7tfbEIP' />
				<div class="form-groups">
					<input name="username" type="text" id="username" placeholder="Username" />
					<div class="username_avail_result" id="username_avail_result"></div>
                    <div class="username-help">USERNAME: MAY NOT CONTAIN CAPITAL LETTERS OR SPECIAL CHARACTERS.</div></div>
                 <div class="form-groups">
					<input name="email" type="text" placeholder="Valid Email Address" />
                    <div class="username-help">Email: PLEASE ENTER A VALID EMAIL ADDRESS</div>
				</div>
				<div class="form-groups">
					<input name="password" type="password" id="password" placeholder="Password" />
                    <div class="password1-help">PASSWORD 1: MUST BE BETWEEN 8-16 CHARACTERS LONG.</div>
                    <div class="password_strength" id="password_strength"></div>
				</div>
				<div class="form-groups">
					<input name="password2" type="password" placeholder="Retype Password" />
                    <div class="password2-help">PLEASE REPEAT PASSWORD.</div>
				</div>
			
				<div class="form-groups">
					<label class="" for="captcha"></label>
                      <div id="captcha-wrap">                
                     <img src="core/captcha.php" id="captcha" Alt="Captcha Code" placeholder="Captcha"/>
					<input name="captcha" type="text" placeholder="Captcha" />
                    <div class="captcha-help">CAPTCHA: THE NUMBERS IN THE BLUE CIRCLE ABOVE</div>
				</div>
              <div class="contacts">beta&nbsp;</div>  
                <div class="form-groups">
					<label>&nbsp;</label>
					<input type="submit" value="Register"/>
				</div>
			</form>
		</div>
	</div>
</body>


</html>