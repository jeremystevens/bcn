<?php
/**
 * load.php
 *
 * @package Blackcat Network
 * @author Jeremy Stevens
 * @copyright 2014 Jeremy Stevens
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */
 
// check if session is already started PHP >= 5.4.0
if(session_id() == '') {
    session_start();
}

 // do action
 function do_the_action($action) {
    switch ($action) {
    	########### login / logout and registration ##############
    	case "register":
    	include_once('core/templates/register.tpl.php');
    	break;	
        # log user in
    	case "login":
    	log_me_in();
    	break;
    	# log user out
    	case "logout":
    	log_me_out();
    	break;
    	# process registration 
    	case "process":
    	process_reg();
    	break;
    	# profile 
    	case "profile";
    	# no action
    	echo "<div class='username-help'>looking for $username</div>";
    	default:
    	include_once('core/templates/main.tpl.php');
    	break;
       ########## Other Site Actions #############
       
    	
  }

 }
 
 ####### Process Registration Requst #############
 function process_reg(){
 	include_once('core/templates/register.tpl.php');
   // clean values 
  $username = clean($_POST['username']);
  $email = clean($_POST['email']);
  $password = clean($_POST['password']);
  $check = isValidEmail($email); 
  // check if email is valid 
  if ($check == false){
  	include_once('core/templates/register.tpl.php');
  	echo "<div class='username-help'> you're email is not valid please enter a valid email</div>";
  	 header('Refresh:3;url=register');
  	exit();
   }
   // check if username is taken & finish 
    check_username($username);
  }
  
   ######### Sanatize and protect ####################
   function clean($value) {

       // If magic quotes not turned on add slashes.
       if(!get_magic_quotes_gpc())

       // Adds the slashes.
       { $value = addslashes($value); }

       // Strip any tags from the value.
       $value = strip_tags($value);

       //  protect against sql injection 
       $value = mysql_real_escape_string($value);
       // Return the value out of the function.
       return $value;
  }
  
  ############# Check if Email is Valid ###############
  function isValidEmail($email)
    {
        //Perform a basic syntax-Check
        //If this check fails, there's no need to continue
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        //extract host
        list($user, $host) = explode("@", $email);
        //check, if host is accessible
        if (!checkdnsrr($host, "MX") && !checkdnsrr($host, "A")) {
            return false;
        }

        return true;
    }
    
  ############ check if user has account or username is taken #################
    function check_username($username){
	 $connection = mysql_connect("localhost","root","")
                or die ("Couldn't connect to server1");
            $db = mysql_select_db("social", $connection)
                or die("Couldn't select database.");
          $query = mysql_query("SELECT username FROM users WHERE username='$username'");
          if (mysql_num_rows($query) != 0)
          {
         include_once('core/templates/register.tpl.php');
  	     echo "<div class='username-help'> Someone already has an account with username $username</div>";
  	     header('Refresh:5;url=register');
  	     unset($username);
  	     exit();
          } 
          else
         {
         echo "<div class='username-help'>Finishing registrationg process..</div>";
          // hash password
     $db_password = md5($password);   
     $token = gen_uniqueIdent($length=32);      
     $sql = mysql_query("INSERT INTO users (username,password,email,token,join_date)
      			VALUES('$username', '$db_password','$email','$token',now())") or die (mysql_error()); 
      if (!$sql) {
            echo "<div class='username-help'>An Error has Occured! the site adminstrator has been notifed </div>";
           $debugme = var_dump($reg);
           $debugme .= "<br>$sql";
           $file = file_get_contents('../LOG.txt', true);
           $debugme .= $file;
           mail($webmaster_email,"Site Error",$debugme); 
        } else {
        	// get the last inserted userid();
        	$userid = mysql_insert_id();
        	if(session_id() == '') {
             session_start();
             }
            //store some variables in session
            $_SESSION['userid'] = $userid;
            $_SESSION['username'] = $username;
             include_once('core/templates/msin.tpl.php');
             echo "<div class='username-help'><font color='red'></i>Your account has been created you may now login</font></i></div>";
           redirect(); 
        	}
   }
         } 

          
  ########### Generate random key #########################      
      // generate random string
 function gen_uniqueIdent($length=32){
    $alphabet = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789');
    $key = '';
    for($loop=0;$loop<$length;$loop++){
        $i = mt_rand(0,count($alphabet)-1);
        $key.= $alphabet[$i];
    };
    return $key;
}        
	
 
   
############# Redirect user ####################
function redirect()
    {
        header('refresh: 6; url=index.php');
        include 'index.php';
    } 
   
?>