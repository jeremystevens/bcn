<?php
/**
 * validate.php  # validates captcha code   
 *
 * @package Blackcat Network
 * @author Jeremy Stevens
 * @copyright 2014 Jeremy Stevens
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */

// check if session is already started PHP >= 5.4.0
if(session_id() == '') {
    session_start();
}

 // now check this code   
 if(isset($_REQUEST['code']))
{
    echo json_encode(strtolower($_REQUEST['code']) == strtolower($_SESSION['captcha']));
}
else
{
    echo 0; // no code
}
?>
