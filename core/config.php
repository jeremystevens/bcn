<?php
/**
 * config.php
 *
 * @package Blackcat Network
 * @author Jeremy Stevens
 * @copyright 2014 Jeremy Stevens
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */
 
 ########## General Info ##########
$sitename = 'BlackCat Network';   # The name of your Site.
$siteindex = "social";    # folder that the pastebin is in on your server
$siteurl = "http://" . $_SERVER['HTTP_HOST']; #  **** do not edit  **** 
$webmaster_email = 'webmaster@email.com';	 # webmaster's' email (like noreply@yourdomain.com.)
$cookiename = 'bc13';		# Name of the cookie to set for authentication.

########## Database Info ##########
$db_type = 'mysql';
$dbhost = 'localhost';
$database_name = 'social';
$dbusername = 'admin';
$dbpasswd = 'password';
$db_persist = 0;
$db_error_send = 1;

########## Directories/Files ##########
# Note: These directories do not have to be changed unless you move things.
$sitedir = dirname(__FILE__); # The absolute path to the phpbin folder. (not just '.'!)
$coredir = dirname(__FILE__) . '/core';


########## Error-Catching ##########
# Note: You shouldn't touch these settings.
$db_last_error = 0;

# should I display site errors? 
$display_errors  = 1;

# this is to log errors to a file
$error_logging = 1;

?>